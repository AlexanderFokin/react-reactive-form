
var PROD = false;
//------------------
var webpack = require("webpack"); 
var path = require('path');
var WebpackShellPlugin = require('webpack-shell-plugin');
//------------------
var config = {
  entry: [
    'babel-polyfill',
    path.resolve(__dirname, 'playground/js/app.js')
  ],
  output: {
    path: path.resolve(__dirname, 'playground/build'),
    filename: 'bundle.js',
    library: 'ReactReactiveForm',
    libraryTarget: 'umd',
    publicPath: "playground/build/"
  },
  externals: {
    // require("jquery") is external and available
    //  on the global var jQuery
  },
  watch: true,
  watchOptions: {
    aggregateTimeout: 100
  },
  devtool: "source-map",
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {presets: ['es2015', 'stage-0', 'react']}
      },
      {test: /\.json$/, loader: 'json'}
    ]
  },
  'uglify-loader': {
    mangle: true
  },
  plugins: PROD ? [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.DefinePlugin({
        'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new WebpackShellPlugin({
      onBuildStart: ['echo "Starting"'],
      onBuildEnd: ['npm run prepublish']
    }),
    new webpack.optimize.UglifyJsPlugin({minimize: true})
  ] : [
    new webpack.optimize.OccurrenceOrderPlugin()
  ]
};

module.exports = config;