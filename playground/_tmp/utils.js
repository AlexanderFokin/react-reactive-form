/**
 * Created by macbook on 23.05.16.
 */


function getIn(obj, path) {
  var stack = path.split('.');

  while ( stack.length ) {
    obj = obj[stack.shift()];
  }

  return obj;
}


function updateIn(obj, path, value) {
  var current = obj;
  var stack = path.split('.');

  while ( stack.length > 1 ) {
    current = current[stack.shift()];
  }
  current[stack.shift()] = value;

  return obj;
}






function setValueToState(context, statePath, _options, value) {

  var options = _.defaults({}, _options, context.constructor.deepLinkeConfig, {
    storeEmptyStringAsNull: false
  });

  if (options.storeEmptyStringAsNull) {

    if (value === '') {
      value = null;
    }
  }

  var updaterObject = {},
    updaterLastOp = updaterObject,
    statePathLastIndex = statePath.length - 1;

  statePath.forEach((part, i) => {

    updaterLastOp = updaterLastOp[part] = {};

    if (i == statePathLastIndex) {
      updaterLastOp.$set = value;
    }
  });

  return update(context.state, updaterObject);
}




function isInt(n){
  return Number(n) === n && n % 1 === 0;
}

function isFloat(n){
  return Number(n) === n && n % 1 !== 0;
}



