/**
 * Created by macbook on 23.05.16.
 */


var lot_res = {
  "default": {
    "title": "Другі",
    "type": "object",
    "required": ["date_and_time_of_the_start_of_trading", "date_and_time_of_the_end_of_trading"],
    "properties": {
      "category_lot": {"type": "string", "title": "Категорія лоту"},
      "the_name_of_the_lot": {"type": "string", "title": "Назва лота"},
      "date_and_time_of_the_start_of_trading": {
        "type": "string",
        "title": "Дата і час початку торгів",
        "format": "date-time"
      },
      "date_and_time_of_the_end_of_trading": {
        "type": "string",
        "title": "Дата і час закінчення торгів",
        "format": "date-time"
      },
      "the_starting_price_of_the_lot": {"type": "number", "title": "Стартова ціна лота"},
      "step_auction": {"type": "number", "minimum": 2, "title": "Крок аукціону"},
      "the_period_of_exposure_lot": {"type": "string", "title": "Період експозиції лота"},
      "type_of_transaction": {"type": "string", "title": "Вид угоди"},
      "location_of_the_lot": {"type": "string", "title": "Місцезнаходження лота"},
      "auction_organizer": {"type": "string", "title": "Організатор аукціону"},
      "sub_lot": {"type": "string", "title": "Будь лота"},
      "features_lot": {"type": "string", "title": "Характеристики лота"},
      "additional_information_about_the_item": {"type": "string", "title": "Додаткова інформація про лот"},
      "expert_regulatory_costs": {"type": "string", "title": "Експертні / Нормативна вартість"},
      "the_need_for_collateral": {"type": "string", "title": "Необхідність гарантійного забезпечення"},
      "bank_details_for_payment_of_collateral": {
        "type": "string",
        "title": "Реквізити для сплати гарантійного забезпечення"
      },
      "essence_of_contract": {"type": "string", "title": "Істотні умови договору"},
      "the_bidder_requirements": {"type": "string", "title": "Вимоги до учасника аукціону"},
      "lot_pictures": {"type": "string", "title": "Зображення лота"},
      "links_to_documents_and_publications_about_the_item": {
        "type": "string",
        "title": "Посилання на документи і публікації про лот"
      }
    }
  },
  "property": {
    "title": "Майно",
    "type": "object",
    "required": ["date_and_time_of_the_start_of_trading", "date_and_time_of_the_end_of_trading"],
    "properties": {
      "category_lot": {"type": "string", "title": "Категорія лоту"},
      "the_name_of_the_lot": {"type": "string", "title": "Назва лота"},
      "date_and_time_of_the_start_of_trading": {
        "type": "string",
        "title": "Дата і час початку торгів",
        "format": "date-time"
      },
      "date_and_time_of_the_end_of_trading": {
        "type": "string",
        "title": "Дата і час закінчення торгів",
        "format": "date-time"
      },
      "the_starting_price_of_the_lot": {"type": "number", "title": "Стартова ціна лота"},
      "step_auction": {"type": "number", "minimum": 2, "title": "Крок аукціону"},
      "the_period_of_exposure_lot": {"type": "string", "title": "Період експозиції лота"},
      "type_of_transaction": {"type": "string", "title": "Вид угоди"},
      "location_of_the_lot": {
        "type": "string",
        "title": "Місцезнаходження лота",
        "properties": {
          "property_location": {"type": "string", "title": "translation missing: ua.property_location"},
          "region": {"type": "string", "title": "translation missing: ua.region"}
        }
      },
      "auction_organizer": {"type": "string", "title": "Замовник продажу об'єкта"},
      "sub_lot": {"type": "string", "title": "Будь лота"},
      "features_lot": {
        "title": "Характеристики лота",
        "type": "object",
        "properties": {
          "object_code_register_number": {"type": "string", "title": "Код об'єкта (реєстровий №)"},
          "area": {"type": "string", "title": "Площа"},
          "dimensions": {"type": "string", "title": "Розмір об’єкта"},
          "main_economic_activity": {"type": "string", "title": "Основний вид економічної діяльності"}
        }
      },
      "additional_information_about_the_item": {
        "type": "object",
        "title": "Додаткова інформація про лот",
        "properties": {
          "code_edrpou_balance_holder": {"type": "string", "title": "Код за ЄДРПОУ балансоутримувача"},
          "name_balance_holder": {"type": "string", "title": "Найменування балансоутримувача"},
          "locate_balance_holder": {"type": "string", "title": "Місцезнаходження балансоутримувача"},
          "information_offer": {"type": "string", "title": "Інформація про об’єкт"}
        }
      },
      "expert_regulatory_costs": {"type": "string", "title": "Експертні / Нормативна вартість"},
      "the_need_for_collateral": {
        "type": "object",
        "title": "Необхідність гарантійного забезпечення",
        "properties": {
          "type_collateral": {
            "type": "string",
            "title": "Финансовая (банк) гарантия, перечисление, Акредитив",
            "enum": ["Financial (bank) guarantee", "transfer", "letter of credit"],
            "enumNames": ["Financial (bank) guarantee", "transfer", "letter of credit"]
          },
          "size_margin": {"type": "string", "title": "Розмір гарантійного внеску"},
          "margin_recipient": {"type": "string", "title": "Отримувач гарантійного внеску"},
          "beneficiary_bank": {"type": "string", "title": "Назва банку отримувача"},
          "the_code_of_the_beneficiary_bank": {"type": "string", "title": "Код банку отримувача (МФО)"},
          "the_account_for_payment_of_the_guarantee_fee": {
            "type": "string",
            "title": "Рахунок отримувача, для сплати гарантійного внеску, №"
          }
        }
      },
      "bank_details_for_payment_of_collateral": {
        "type": "string",
        "title": "Реквізити для сплати гарантійного забезпечення"
      },
      "essence_of_contract": {"type": "string", "title": "Істотні умови договору"},
      "the_bidder_requirements": {"type": "string", "title": "Вимоги до учасника аукціону"},
      "lot_pictures": {"type": "string", "title": "Зображення лота"},
      "links_to_documents_and_publications_about_the_item": {
        "type": "string",
        "title": "Посилання на документи і публікації про лот"
      }
    }
  },
  "land": {
    "title": "Земельні ділянки",
    "type": "object",
    "required": ["date_and_time_of_the_start_of_trading", "date_and_time_of_the_end_of_trading"],
    "properties": {
      "category_lot": {"type": "string", "title": "Категорія лоту"},
      "the_name_of_the_lot": {"type": "string", "title": "Назва лота"},
      "date_and_time_of_the_start_of_trading": {
        "type": "string",
        "title": "Дата і час початку торгів",
        "format": "date-time"
      },
      "date_and_time_of_the_end_of_trading": {
        "type": "string",
        "title": "Дата і час закінчення торгів",
        "format": "date-time"
      },
      "the_starting_price_of_the_lot": {"type": "number", "title": "Стартова ціна лота"},
      "step_auction": {"type": "number", "minimum": 2, "title": "Крок аукціону"},
      "the_period_of_exposure_lot": {"type": "string", "title": "Період експозиції лота"},
      "type_of_transaction": {"type": "string", "title": "Вид угоди"},
      "location_of_the_lot": {"type": "string", "title": "Місцезнаходження лота"},
      "auction_organizer": {"type": "string", "title": "Організатор земельного аукциона"},
      "sub_lot": {"type": "string", "title": "Будь лота"},
      "features_lot": {
        "title": "Характеристики лота",
        "type": "object",
        "properties": {
          "number": {"type": "string", "title": "Кадастровий номер земельної ділянки"},
          "square": {"type": "string", "title": "Площа"}
        }
      },
      "additional_information_about_the_item": {"type": "string", "title": "Додаткова інформація про лот"},
      "expert_regulatory_costs": {"type": "string", "title": "Експертні / Нормативна вартість"},
      "the_need_for_collateral": {
        "type": "object",
        "title": "Необхідність гарантійного забезпечення",
        "properties": {
          "type_collateral": {
            "type": "string",
            "title": "Финансовая (банк) гарантия, перечисление, Акредитив"
          },
          "size_margin": {"type": "string", "title": "Розмір гарантійного внеску"},
          "margin_recipient": {"type": "string", "title": "Отримувач гарантійного внеску"},
          "beneficiary_bank": {"type": "string", "title": "Назва банку отримувача"},
          "the_code_of_the_beneficiary_bank": {"type": "string", "title": "Код банку отримувача (МФО)"},
          "the_account_for_payment_of_the_guarantee_fee": {
            "type": "string",
            "title": "Рахунок отримувача, для сплати гарантійного внеску, №"
          }
        }
      },
      "bank_details_for_payment_of_collateral": {
        "type": "string",
        "title": "Реквізити для сплати гарантійного забезпечення"
      },
      "essence_of_contract": {"type": "string", "title": "Істотні умови договору"},
      "the_bidder_requirements": {"type": "string", "title": "Вимоги до учасника аукціону"},
      "lot_pictures": {"type": "string", "title": "Зображення лота"},
      "links_to_documents_and_publications_about_the_item": {
        "type": "string",
        "title": "Посилання на документи і публікації про лот"
      }
    }
  },
  "auto": {
    "title": "Авто і мото",
    "type": "string",
    "required": ["date_and_time_of_the_start_of_trading", "date_and_time_of_the_end_of_trading"],
    "properties": {
      "category_lot": {"type": "string", "title": "Категорія лоту"},
      "the_name_of_the_lot": {"type": "string", "title": "Назва лота"},
      "date_and_time_of_the_start_of_trading": {
        "type": "string",
        "title": "Дата і час початку торгів",
        "format": "date-time"
      },
      "date_and_time_of_the_end_of_trading": {
        "type": "string",
        "title": "Дата і час закінчення торгів",
        "format": "date-time"
      },
      "the_starting_price_of_the_lot": {"type": "number", "title": "Стартова ціна лота"},
      "step_auction": {"type": "number", "minimum": 2, "title": "Крок аукціону"},
      "the_period_of_exposure_lot": {"type": "string", "title": "Період експозиції лота"},
      "type_of_transaction": {"type": "string", "title": "Вид угоди"},
      "location_of_the_lot": {"type": "string", "title": "Місцезнаходження лота"},
      "auction_organizer": {"type": "string", "title": "Організатор аукціону"},
      "sub_lot": {"type": "string", "title": "Будь лота"},
      "features_lot": {"type": "string", "title": "Характеристики лота"},
      "additional_information_about_the_item": {"type": "string", "title": "Додаткова інформація про лот"},
      "expert_regulatory_costs": {"type": "string", "title": "Експертні / Нормативна вартість"},
      "the_need_for_collateral": {"type": "string", "title": "Необхідність гарантійного забезпечення"},
      "bank_details_for_payment_of_collateral": {
        "type": "string",
        "title": "Реквізити для сплати гарантійного забезпечення"
      },
      "essence_of_contract": {"type": "string", "title": "Істотні умови договору"},
      "the_bidder_requirements": {"type": "string", "title": "Вимоги до учасника аукціону"},
      "lot_pictures": {"type": "string", "title": "Зображення лота"},
      "links_to_documents_and_publications_about_the_item": {
        "type": "string",
        "title": "Посилання на документи і публікації про лот"
      }
    },
    "label": "Авто і мото"
  }
};
