/**
 * Created by macbook on 22.05.16.
 */

import React, { Component, PropTypes } from 'react';
//import DeepLinkMixin from 'react-deep-link';
//import DeepLinkDecorator from 'react-deep-link/lib/decorator';
import _ from 'lodash';
import update from 'react-addons-update';




const TextInput = (props) => {
  const handleChange = (e) => props.onChange(e.target.value);

  return (
    <input
      {...props}
      onChange={handleChange}
    />
  );
};

const DateInput = (props) => {
  const handleChange = (e) => props.onChange(e.target.value);

  return (
    <input
      {...props}
      onChange={handleChange}
    />
  );
};

const Html = (props) => {
  const handleChange = (e) => props.onChange(e.target.value);

  return (
    <div className="html">
      <div>{}</div>
    </div>
  );
};

const Select = (props) => {
  const handleChange = (e) => props.onChange(e.target.value);

  let {data} = props;

  let list;
  if (_.isArray(data.options)) {
    list = _.map(data.options, (value, index) => {
      //console.log(value);
      return <option key={index} value={value.value}>{value.label}</option>
    })
  }

  return (
    <div>
      <select {...props} onChange={handleChange}>
        <option value="">placeholder</option>
        {list}
      </select>
    </div>
  );
};

const Radio = (props) => {
  const handleChange = (e) => props.onChange(e.target.value);

  let {data, name} = props;

  let list;
  if (_.isArray(data.options)) {
    list = _.map(data.options, (value, index) => {
      return (
        <label htmlFor={name + '_' + index} key={index}>
          <input type="radio" id={name + '_' + index} name={name} value={value.value} onChange={handleChange}/>
          {value.label}
        </label>
      )
    })
  }

  return (
    <div>
      {list}
    </div>
  );
};

const Checkbox = (props) => {
  const handleChange = (e) => props.onChange(e.target.checked);

  let {data, name, state} = props;
  
  return (
    <div>
      <input type="checkbox"
        {...props}
             name={name}
             checked={state[name]}
             onChange={handleChange}
      />
    </div>
  );
};

const UnsupportedField = (props) => {
  return (
    <div>
      <h1>UnsupportedField</h1>
    </div>
  );
};


const COMPONENT_TYPES = {
  "input.text":   TextInput,
  "input.date":   DateInput,
  "select":       Select,
  "radio":        Radio,
  "checkbox":     Checkbox
};


function getFieldComponent(type) {
  return COMPONENT_TYPES[type] || UnsupportedField;
}


function iterate(obj, stack, callback) {
  for (var property in obj) {
    if (obj.hasOwnProperty(property) ) {

      if(obj[property].type) {
        var path = stack ? (stack + '.' + property) : property;
        //console.log('key: ' + property);
        //console.log('path: ' + path);

        callback(property, path, obj[property]);

      }else{
        var path = stack ? (stack + '.' + property) : property;
        iterate(obj[property],path, callback );
      }

    }
  }
}

function updateIn(obj, path, value) {
  var current = obj;
  var stack = path.split('.');

  while ( stack.length > 1 ) {
    current = current[stack.shift()];
  }
  current[stack.shift()] = value;

  return obj;
}
var createNestedObject = function( base, names ) {
  for( var i = 0; i < names.length; i++ ) {
    base = base[ names[i] ] = base[ names[i] ] || {};
  }
};

function isObject(thing) {
  return typeof thing === "object" && thing !== null && !Array.isArray(thing);
}
function mergeObjects(obj1, obj2, concatArrays = false) {
  // Recursively merge deeply nested objects.
  var acc = Object.assign({}, obj1); // Prevent mutation of source object.
  return Object.keys(obj2).reduce((acc, key) =>{
    const left = obj1[key], right = obj2[key];
    if (obj1.hasOwnProperty(key) && isObject(right)) {
      acc[key] = mergeObjects(left, right, concatArrays);
    } else if (concatArrays && Array.isArray(left) && Array.isArray(right)) {
      acc[key] = left.concat(right);
    } else {
      acc[key] = right;
    }
    return acc;
  }, acc);
}

class FormBuilder extends Component {
  constructor(props) {
    super(props);
    // Initialize with an empty state to prevent errors
    this.state = {
      fields: props.schema.fields,
      rules: props.schema.rules,
      data: {
        lot_name: ''
      }
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();

    this.props.onSubmit(this.state);
  }

  handleChange(name, value, path) {
    console.log('handleChange[ name: ' + name + ', value: ' + value + ' path: ' + path + ' ]');
    
    // if(_.has(this.state.rules, name)){
    //   console.log(this.state.rules[name]);
    //
    //   _.forEach(this.state.rules[name], function(rule, index) {
    //
    //     console.log(rule);
    //
    //   });
    //
    // }

    //let _data = {...this.state.data};
    //let new_data = updateIn(_data, path, value);
    //console.log(new_data);
    //let data = {...this.state.data, [name]: value };
    //return this.setState({data: data}, () => {
    //  if (this.props.onChange) this.props.onChange(this.state);
    //});

    // createNestedObject( data, ["shapes", "triangle", "title"] );

    path = "data."+path;
    var statePath;
    if (typeof path == "string") {
      statePath = path.split(/[\.\[\]]/g);
    }

    console.log(statePath);

    /*
    //var newState = setValueToState(this, statePath, options, value)

    var updaterObject = {},
      updaterLastOp = updaterObject,
      statePathLastIndex = statePath.length - 1;

    statePath.forEach((part, i) => {

      updaterLastOp = updaterLastOp[part] = {};

      if (i == statePathLastIndex) {
        console.log(updaterLastOp);
        //updaterLastOp.$set = value;

        updaterLastOp = value;
      }
    });

    //console.log(value);
    console.log(updaterObject);
*/

    var updaterObject = {...this.state};

    createNestedObject( updaterObject, statePath );

    updaterObject = updateIn(updaterObject, path, value);
    var newState = mergeObjects(this.state, updaterObject, false);

    //var newState = update(this.state, updaterObject);

    console.log(newState);

    this.setState(newState, () => {
      console.log(this.state);
      if (this.props.onChange) this.props.onChange(this.state);
    });


  }

  render() {

    let schema = this.props.schema;

    let form = [];
    var that = this;


    iterate(this.state.fields, '', function (key, path, value) {

      const FieldComponent = getFieldComponent(value.type);
      form.push(<FieldComponent key={key}
                                name={path}
                                data={value}
                                state={that.state}
                                
                                onChange={value => { that.handleChange(key, value, path);}}
      />);
      
    });
    
    
    /*
    _.forEach(this.state.fields, function(value, key) {
      if(_.isObject(value)) {

        const FieldComponent = getFieldComponent(value.type);
        form.push(<FieldComponent key={key}
                                  name={key}
                                  data={value}
                                  state={that.state}
                                  onChange={value => { that.handleChange(key, value);}}
        />);

        //console.log('key: ' + key + ' value: ' + value + ' type: ' + value.type);

      }

    });
*/


    return (
      <form onSubmit={this.handleSubmit}>
        {this.props.error ? (
          <div className="form-container-error">
            {this.props.error}
          </div>
        ) : null}

        { form }

        <button type="submit">submit</button>
      </form>
    );
  }
}


export default FormBuilder;