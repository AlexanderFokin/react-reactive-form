/**
 * Created by macbook on 18.05.16.
 */

import React, {PropTypes} from 'react'
import Form from 'react-input'



let TextInput = React.createClass({
  render() {
    let props = {};
    props.type = "text";
    //props.name = this.props.n;

    return(
      <div>
          <input {...props} />
      </div>
    );
  }
});


function build(schema, context) {
  let form = [];
  _.forEach(schema, function(value, key) {
    if(_.isObject(value)){

      console.log('key: ' + key + ' value: ' + value + ' type: ' + value.type);

      switch (value.type) {
        case "input.text":
          form.push(<TextInput ref={key} handleChange={context.handleChange} key={key} name={key} data={value} />);
          break;
      }

    }

  });

  console.log(form);

  return form;
}


const FormBuilder = React.createClass({
  getInitialState() {
    return {
      tab: 0
    };
  },
  getDefaultProps() {
    return {
      test: 'test'
    };
  },
  handleChange: function (e) {
    console.log('handleChange');
    console.log(e);
  },
  render() {
    var {schema} = this.props;
    console.log(schema);
    let form = build(schema.auctions[0], this);
    // let form = [];
    // _.forEach(schema.auctions[0], function(value, key) {
    //   if(_.isObject(value)){
    //
    //     console.log('key: ' + key + ' value: ' + value + ' type: ' + value.type);
    //
    //     switch (value.type) {
    //       case "input.text":
    //         form.push(<TextInput ref={key} handleChange={this.handleChange} key={key} name={key} data={value} />);
    //         break;
    //     }
    //
    //   }
    //
    // });

    return (
      <span>
        {form}
      </span>
    );
  }
});


export default FormBuilder;