/**
 * Created by macbook on 24.05.16.
 */
var _ = require('lodash');

const formSchema = {
  fields: {
    //type: 'fields',
    lot_name: {
      "type": "input.text",
      "label": "Title",
      "description": "Description field with id field_title.",
      "placeholder": "Please enter ....",
      required: 'required',
      "enabled": true,
      "hidden": false,

    },
    details_100500:{
      field_100500: {
        "type": "checkbox",
        "label": "checkbox",
        "description": "Select preferred date format.",
        "default": "",
        "enabled": true,
        "hidden": false
      }
    },
    start_date: {
      "type": "input.date",
      "label": "Start date",
      "description": "Select preferred date format.",
      "default": "dd-MM-YYYY",
      "enabled": true,
      "hidden": false
    },
    end_date: {
      "type": "input.date",
      "label": "End date",
      "description": "Select preferred date format.",
      "default": "dd-MM-YYYY",
      "enabled": true,
      "hidden": false
    },
    type_lot: {
      "type": "select",
      "label": "select",
      "description": "Select preferred date format.",
      "default": "",
      "enabled": true,
      "hidden": false,
      "options": [
        {label: "option 0", value: 0},
        {label: "option 1", value: 1},
        {label: "option 2", value: 2}
      ]
    },
    type_lot2: {
      "type": "radio",
      "label": "radio",
      "description": "Select preferred date format.",
      "default": "",
      "enabled": true,
      "hidden": false,
      "options": [
        {label: "option 0", value: 0},
        {label: "option 1", value: 1},
        {label: "option 2", value: 2}
      ]
    },
    confirm: {
      "type": "checkbox",
      "label": "checkbox",
      "description": "Select preferred date format.",
      "default": "",
      "enabled": true,
      "hidden": false
    },
    details:{
      //type: 'fields',
      field_1: {
        "type": "checkbox",
        "label": "checkbox",
        "description": "Select preferred date format.",
        "default": "",
        "enabled": true,
        "hidden": false
      },
      field_2: {
        "type": "input.text",
        "label": "text",
        "description": "Select preferred date format.",
        "default": "",
        "enabled": true,
        "hidden": false
      },
      sub_details: {
        //type: 'fields',
        field_3: {
          "type": "checkbox",
          "label": "checkbox",
          "description": "Select preferred date format.",
          "default": "",
          "enabled": true,
          "hidden": false
        },
        field_4: {
          "type": "input.text",
          "label": "text",
          "description": "Select preferred date format.",
          "default": "",
          "enabled": true,
          "hidden": false
        }
      }
    }

  },
  rules: {
    confirm: [
      {
        on: 'change',
        change: [

        ]

      }
    ]
  }

};




function _build(fields) {


    Object.keys(fields).reduce((acc, key) => {
      //acc[key] = computeDefaults(schema.properties[key], (defaults || {})[key], definitions);

      console.log(key);
      //console.log(acc);

      //acc[key] = formSchema.fields[key];
      // acc[key] = _build(fields[key]);

      if(fields[key].type) {

      }else{
        //var path = stack ? (stack + '.' + property) : property;
        //iterate(obj[property],path, callback );
        acc[key] = _build(fields[key]);
      }


      return acc;
    }, {});

}



//build(formSchema.fields);



_build(formSchema.fields, '');





