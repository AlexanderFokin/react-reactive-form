/**
 * Created by macbook on 26.05.16.
 */

// DateTime

import React, { Component } from 'react';
import MaskedInput from 'react-maskedinput';
import moment from 'moment';


const DateTime = (props) => {
  const handleChange = (e) => {

    var value_f = moment(e.target.value, 'YYYY-MM-DD HH:mm');
    var value_u = value_f.unix();


    if(value_f.isValid()){
      props.onChange(value_u);
    }

    //console.log(e.target.value);
    console.log(value_f);
    console.log(value_u);

    props.onChange(value_u);
  };

  //console.log(props);
  var displayValue, dt;
  if(Number(props.value) === props.value && props.value % 1 === 0){
    dt = moment.unix(props.value);
  }else if(typeof props.value == 'string'){
    dt = moment(props.value, 'YYYY-MM-DD HH:mm');
    console.log(dt);
    console.log(dt.invalidAt());
  }

  console.log(props.value);

  if(dt.isValid()){
    displayValue = dt.format('YYYY-MM-DD HH:mm');
  }else{
    displayValue = props.value;
  }

  console.log(displayValue);

  return (
    <div className={props.classes}>
      <label htmlFor="">{props.data.label}</label>

      <MaskedInput
        mask="1111-11-11 11:11"
        placeholder="YYYY-MM-DD HH:mm"
        placeholderChar=' '
        type="text"
        value={displayValue}
        onChange={handleChange}
      />

    </div>
  );
};

export default DateTime;


/*
 <input
 {...props.data}
 value={props.value}
 className="form-control"
 onChange={handleChange}
 />
 */