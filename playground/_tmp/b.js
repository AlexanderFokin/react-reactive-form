/**
 * Created by macbook on 24.05.16.
 */



switch (value.type) {
  case "input.text":
    form.push(<TextInput  key={key} name={key} data={value}
                          onChange={value => {
              that.handleChange(key, value);
            }}/>);
    break;
  case "input.date":
    form.push(<DateInput  key={key} name={key} data={value}
                          onChange={value => {
              that.handleChange(key, value);
            }}/>);
    break;
  case "select":
    form.push(<Select  key={key} name={key} data={value}
                       onChange={value => {
              that.handleChange(key, value);
            }}/>);
    break;
  case "radio":
    form.push(<Radio  key={key} name={key} data={value}
                      onChange={value => {
              that.handleChange(key, value);
            }}/>);
    break;
  case "checkbox":
    form.push(<Checkbox  key={key} name={key} data={value} state={that.state}
                         onChange={value => {
              that.handleChange(key, value);
            }}/>);
    break;
}

Object.byString = function(o, s) {
  s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
  s = s.replace(/^\./, '');           // strip a leading dot
  var a = s.split('.');
  for (var i = 0, n = a.length; i < n; ++i) {
    var k = a[i];
    if (k in o) {
      o = o[k];
    } else {
      return;
    }
  }
  return o;
}



Object.resolve = function(path, obj, safe) {
  return path.split('.').reduce(function(prev, curr) {
    return !safe ? prev[curr] : (prev ? prev[curr] : undefined)
  }, obj || self)
};
//Object.resolve('properties.that.do.not.exist', {hello:'world'}, true)


function iterate(obj, stack) {
  for (var property in obj) {
    if (obj.hasOwnProperty(property) ) {

      if (typeof obj[property] == "object") { // type: 'fields',

        iterate(obj[property], stack + '.' + property);

      } else if(obj[property] != 'fields'){
        //console.log(property + "   " + obj[property]);
        //console.log(stack + '.' + property);
        console.log(stack);

      }

    }
  }
}

function _iterate(obj, stack) {
  for (var property in obj) {
    if (obj.hasOwnProperty(property) ) {

      if(obj[property].type) {
        //console.log('key: ' + property + ' | type: ' + obj[property].type + ' | path: ' + stack + '.' + property);

        var path = stack ? (stack + '.' + property) : property;
        console.log('key: ' + property);
        console.log('path: ' + path);
        console.log('type: ' + obj[property].type);
        console.log('-------------------------');

      }else{
        var path = stack ? (stack + '.' + property) : property;
        iterate(obj[property],path );
      }

    }
  }
}

function mergeObjects(obj1, obj2, concatArrays = false) {
  // Recursively merge deeply nested objects.
  var acc = Object.assign({}, obj1); // Prevent mutation of source object.
  return Object.keys(obj2).reduce((acc, key) =>{
    const left = obj1[key], right = obj2[key];
    if (obj1.hasOwnProperty(key) && isObject(right)) {
      acc[key] = mergeObjects(left, right, concatArrays);
    } else if (concatArrays && Array.isArray(left) && Array.isArray(right)) {
      acc[key] = left.concat(right);
    } else {
      acc[key] = right;
    }
    return acc;
  }, acc);
}

function build(fields, stack) {

  _.forEach(fields, function(value, key) {
    //var path =

    if(value.type) {
      var path = stack? stack : key;
      console.log('key: ' + key + ' value: ' + value + ' type: ' + value.type + ' path: ' + path);

    }else{
      stack = stack? (stack + '.' + key) : key;
      build(fields[key], stack );
    }

  });

}



// @flow

function foo(x: string, y: number): string {
  return x.length * y;
}

foo('Hello', 42);

