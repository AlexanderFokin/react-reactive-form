

export const formSchema = {
  "fields": {
    "lot_name": {
      "type": "input.text",
      "label": "Name",
      "description": "Description field with id field_title.",
      "placeholder": "Please enter ....",
      "default": 'Lot name',
      "required": false,
      "enabled": true,
      "hidden": false
    },
    "price": {
      "type": "input.currency",
      "label": "price",
      "description": "price.",
      "placeholder": "price ....",
      "default": 0,
      "required": false,
      "enabled": true,
      "hidden": false
    },
    confirm: {
      "type": "checkbox",
      "label": "checkbox",
      "description": "Select preferred date format.",
      "default": false,
      "enabled": true,
      "hidden": false,
      "onchange": function (value, state) {

        if (value) {
          state.fields.lot_name.enabled = false
        } else {
          state.fields.lot_name.enabled = true
        }

        return state;
      }
    },
    images: {
      "type": "upload",
      "label": "Загрузить Фото",
      "description": "Загрузить Фото.",
      "default": "",
      "enabled": true,
      "hidden": false,
    },
    start_date: {
      "type": "input.datetime",
      "label": "Start date",
      "description": "Select preferred date format.",
      "default": "",
      "format": "YYYY-MM-DD HH:mm",
      "mask": "1111-11-11 11:11",
      "required": false,
      "enabled": true,
      "hidden": false,
    },
    end_date: {
      "type": "input.datetime",
      "label": "End date",
      "description": "Select preferred date format.",
      "default": "",
      "format": "YYYY-MM-DD",
      "mask": "1111-11-11",
      "enabled": true,
      "hidden": false
    },
    type_lot: {
      "type": "select",
      "label": "select",
      "description": "Select preferred date format.",
      "default": 1,
      "enabled": true,
      "hidden": false,
      "required": false,
      "options": [
        {label: "option 0", value: 0},
        {label: "option 1", value: 1},
        {label: "option 2", value: 2}
      ]
    },
    categories: {
      "type": "checkbox.group",
      "label": "checkbox.group",
      "description": "checkbox group",
      "default": '',
      "enabled": true,
      "hidden": false,
      "required": true,
      "options": [
        {label: "CHILDREN", value: 'CHILDREN'},
        {label: "FREE", value: 'FREE'},
        {label: "THEATRES", value: 'THEATRES'},
        {label: "CINEMA", value: 'CINEMA'},
        {label: "EXPO", value: 'EXPO'},
        {label: "SHOW", value: 'SHOW'}
      ],
      place: "experimental-component"
    },
    geo_position: {
      "type": "geoposition",
      "label": "geoposition",
      "description": "geoposition",
      "default": "",
      "enabled": true,
      "hidden": false,
      "latitude": 50.4016974,
      "longitude": 30.2518256,
      "map": {
        "enabled": true,
        "width": "400px",
        "height": "400px"
      },
      place: "experimental-component-1"
    },
    type_lot2: {
      "type": "radio",
      "label": "radio",
      "description": "Select preferred date format.",
      "default": 2,
      "enabled": true,
      "hidden": false,
      "options": [
        {label: "Продажа", value: 0},
        {label: "Аренда", value: 1},
        {label: "Лизинг", value: 2}
      ],
      "onchange": function (value, state) {

        if (value == 1) {
          state.fields.orenda_date.enabled = true
        } else {
          state.fields.orenda_date.enabled = false
        }

        return state;
      }
    },
    orenda_date: {
      "type": "input.datetime",
      "label": "Аренда c (дата)",
      "description": "Select preferred date format.",
      "default": "",
      "enabled": false,
      "hidden": false
    },
    details: {
      field_1: {
        "type": "checkbox",
        "label": "Скрыть поле (Номер счета)",
        "description": "Select preferred date format.",
        "default": "",
        "enabled": true,
        "hidden": false,
        place: 'place-0',
        "onchange": function (value, state) {

          if (value) {
            state.fields.details.field_1.label = "Показать поле (Номер счета)"
            state.fields.details.field_2.enabled = false
          } else {
            state.fields.details.field_1.label = "Скрыть поле (Номер счета)"
            state.fields.details.field_2.enabled = true
          }

          return state;
        }
      },
      field_2: {
        "type": "input.text",
        "label": "Номер счета",
        "placeholder": "4111 1111 1111 1111",
        "description": "Select preferred date format.",
        "default": "",
        "enabled": true,
        "hidden": false,
        place: 'place-0'
      },
      sub_details: {
        field_3: {
          "type": "checkbox",
          "label": "Переместить поле (Просто текст) в таб",
          "description": "Select preferred date format.",
          "default": "",
          "enabled": true,
          "hidden": false,
          place: 'place-1',
          "onchange": function (value, state) {

            if (value) {
              state.fields.details.sub_details.field_3.label = "Вернуть поле (Просто текст) в таб"
              state.fields.details.sub_details.field_4.place = 'place-0'
            } else {
              state.fields.details.sub_details.field_3.label = "Переместить поле (Просто текст) в таб"
              state.fields.details.sub_details.field_4.place = 'place-1'
            }

            return state;
          }
        },
        field_4: {
          "type": "input.text",
          "label": "Просто текст",
          "description": "Select preferred date format.",
          "default": "",
          "enabled": true,
          "hidden": false,
          place: 'place-1'
        }
      }
    }

  },
  places: {
    "default": {
      title: "Main info"
    },
    "place-0": {
      title: "Main info 2"
    },
    "place-1": {
      title: "More info"
    },
    "experimental-component": {
      title: "Experimental component"
    },
    "experimental-component-1": {
      title: "Experimental component Geo Position"
    }
  }

};


