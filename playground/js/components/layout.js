
import React, {PropTypes} from 'react'
import {Link} from 'react-router'

//Container
const Layout = React.createClass({
  _handleLogout: function(){
    this.props.userActions.logout();
  },
  render: function () {
    const {dispatch, lots, user} = this.props;
    
    return (
      <div>
        <Link to="/form">Form</Link>
        <br/>
        {this.props.children}
      </div>
    );
  }
});


export default Layout