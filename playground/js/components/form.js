import React, {PropTypes} from 'react'
import FormBuilder from '../../../src/index';
import JSONTree from './json-viewer/jsonViewer';

import {formSchema} from '../schema';


/*
 TODO: Warning: setState(...): Cannot update during an existing state transition (such as within `render` or another component's constructor).
 Render methods should be a pure function of props and state; constructor side-effects are an anti-pattern,
 but can be moved to `componentWillMount`.
 */


const Form = React.createClass({

  getInitialState: function () {
    return {
      data: {}
    }
  },

  handleSubmit: function (state) {
    console.log('handleSubmit');
    console.log(state);
    //this.setState({form: state});
  },

  handleChange: function (state) {
    console.log(state);
    //this.setState({data: state});
  },

  componentWillMount: function () {

  },

  render: function () {
    const {dispatch, lots, user} = this.props;

    return (
      <div>
        <h1>React Reactive Form</h1>
        <br/>

        <FormBuilder schema={formSchema}
                     debug={true}
                     onChange={this.handleChange}
                     onSubmit={this.handleSubmit}/>

        <br/>

        {/*
        <div className="json_tree_wrap">
          <JSONTree data={form}/>
        </div>
         */}

      </div>
    );
  }
});


export default Form