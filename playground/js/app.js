/**
 * Created by alexander on 12.04.16.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory, hashHistory, Router, Route, Link, IndexRoute } from 'react-router'

import Layout from './components/layout';
import Form from './components/form';

ReactDOM.render((
  <Router history={hashHistory}>
    <Route path="/" component={Layout}>
      <IndexRoute component={Form}/>
      <Route path="/form" component={Form}/>
    </Route>
  </Router>
), document.getElementById('app'));




