/**
 * Created by macbook on 30.05.16.
 */

var _ = require('lodash');

var imgs = [
    {
      bytes: 24878,
      created_at: "2016-05-30T15:26:34Z",
      etag: "34901ba22f422469d2e4dcf15daeaaba",
      format: "png",
      height: 500,
      original_filename: "learning-react-real-time-node",
      path: "v1464621994/ribi7uz9mzudhjwrxqfa.png",
      public_id: "ribi7uz9mzudhjwrxqfa",
      resource_type: "image",
      secure_url: "https://res.cloudinary.com/dkcebtqro/image/upload/v1464621994/ribi7uz9mzudhjwrxqfa.png",
      signature: "316bcb63c895392961485ff2287240ad5ad63725",
      thumbnail_url: "http://res.cloudinary.com/dkcebtqro/image/upload/c_limit,h_60,w_90/v1464621994/ribi7uz9mzudhjwrxqfa.png",
      type: "upload",
      url: "http://res.cloudinary.com/dkcebtqro/image/upload/v1464621994/ribi7uz9mzudhjwrxqfa.png",
      version: 1464621994,
      width: 1200,
    },
    {
      bytes: 24878,
      created_at: "2016-05-30T15:26:34Z",
      etag: "34901ba22f422469d2e4dcf15daeaaba",
      format: "png",
      height: 500,
      original_filename: "learning-react-real-time-node",
      path: "v1464621994/ribi7uz9mzudhjwrxqfa.png",
      public_id: "ribi7uz9mzudhjwrxqfa",
      resource_type: "image",
      secure_url: "https://res.cloudinary.com/dkcebtqro/image/upload/v1464621994/ribi7uz9mzudhjwrxqfa.png",
      signature: "316bcb63c895392961485ff2287240ad5ad63725",
      thumbnail_url: "http://res.cloudinary.com/dkcebtqro/image/upload/c_limit,h_60,w_90/v1464621994/ribi7uz9mzudhjwrxqfa.png",
      type: "upload",
      url: "http://res.cloudinary.com/dkcebtqro/image/upload/v1464621994/ribi7uz9mzudhjwrxqfa.png",
      version: 1464621994,
      width: 1200,
    },
    {
      bytes: 24878,
      created_at: "2016-05-30T15:26:34Z",
      etag: "34901ba22f422469d2e4dcf15daeaaba",
      format: "png",
      height: 500,
      original_filename: "learning-react-real-time-node",
      path: "v1464621994/ribi7uz9mzudhjwrxqfa.png",
      public_id: "ribi7uz9mzudhjwrxqfa",
      resource_type: "image",
      secure_url: "https://res.cloudinary.com/dkcebtqro/image/upload/v1464621994/ribi7uz9mzudhjwrxqfa.png",
      signature: "316bcb63c895392961485ff2287240ad5ad63725",
      thumbnail_url: "http://res.cloudinary.com/dkcebtqro/image/upload/c_limit,h_60,w_90/v1464621994/ribi7uz9mzudhjwrxqfa.png",
      type: "upload",
      url: "http://res.cloudinary.com/dkcebtqro/image/upload/v1464621994/ribi7uz9mzudhjwrxqfa.png",
      version: 1464621994,
      width: 1200,
    },
  ];


var res = _.reduce(imgs, function(result, value, key) {
  result.push(value.url);
  return result;
}, []);


console.log(res);