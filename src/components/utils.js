
import React, { Component } from 'react';
import _ from 'lodash';

export function getIn(obj, path) {
  var stack;

  if (typeof path == "string") {
    stack = path.split(/[\.\[\]]/g);
  }else{
    stack = path;
  }

  while ( stack.length ) {
    obj = obj[stack.shift()];
  }

  return obj;
}


export function updateIn(obj, path, value) {
  var current = obj;
  var stack;

  if (typeof path == "string") {
    stack = path.split(/[\.\[\]]/g);
  }else{
    stack = path;
  }

  while ( stack.length > 1 ) {
    current = current[stack.shift()];
  }
  current[stack.shift()] = value;

  return obj;
}

export function createNestedObject( base, names ) {
  for( var i = 0; i < names.length; i++ ) {
    base = base[ names[i] ] = base[ names[i] ] || {};
  }
}


export function iterate(obj, stack, callback) {
  for (var property in obj) {
    if (obj.hasOwnProperty(property) ) {

      if(obj[property].type) {
        var path = stack ? (stack + '.' + property) : property;
        callback(property, path, obj[property]);

      }else{
        var path = stack ? (stack + '.' + property) : property;
        iterate(obj[property],path, callback );
      }

    }
  }
}


function isObject(thing) {
  return typeof thing === "object" && thing !== null && !Array.isArray(thing);
}
export function mergeObjects(obj1, obj2, concatArrays = false) {
  // Recursively merge deeply nested objects.
  var acc = Object.assign({}, obj1); // Prevent mutation of source object.
  return Object.keys(obj2).reduce((acc, key) =>{
    const left = obj1[key], right = obj2[key];
    if (obj1.hasOwnProperty(key) && isObject(right)) {
      acc[key] = mergeObjects(left, right, concatArrays);
    } else if (concatArrays && Array.isArray(left) && Array.isArray(right)) {
      acc[key] = left.concat(right);
    } else {
      acc[key] = right;
    }
    return acc;
  }, acc);
}


export function getPath(path) {

  var ps = "data."+path;
  var pf = "fields."+path;

  return {
    state: ps.split(/[\.\[\]]/g),
    field: pf.split(/[\.\[\]]/g)
  };
}



export function fieldClasses(field) {
  return [
    'form-group',
    `field-${field.key}`,
    `field-visible-${field.hidden}`,
    `field-type-${field.type.replace('.', '_')}`,
  ].join(" ").trim();
}


export function changeState(context, state, newState, callback) {
  var ns = mergeObjects(state, newState, false);

  context.setState(ns, () => {
    //if (this.props.onChange) callback(this.state);
  });
}


export function defaultDataState(fields) {
  var data = {};
  iterate(fields, '', function (key, path, value) {
    var statePath = path.split(/[\.\[\]]/g);
    createNestedObject( data, statePath );
    if(_.has(value, 'default')){
      data = updateIn(data, statePath, value.default);
    }else{
      data = updateIn(data, statePath, '');
    }
  });
  return data;
}

// create form parts tabs etc
export function setPlaces(places, schemaPlaces) {
  var elements = [];
  _.forEach(places, function(fields, key) {
    let header;
    let title;
    let description;

    if(_.has(schemaPlaces, key)){
      title = (schemaPlaces[key].title)? <div className="place-title">{schemaPlaces[key].title}</div> : '';
      description = (schemaPlaces[key].description)? <p className="place-description">{schemaPlaces[key].description}</p> : '';
      header = <div className="place-header">{title} {description}</div>;
    }

    elements.push(
      <div className={'form-part ' + key} key={key}>
        {header}
        {fields}
      </div>
    )
  });
  return elements;
}

