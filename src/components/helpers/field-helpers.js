
import React, { Component } from 'react';

export function isRequired(required) {
  return (required) ? <span className="required">*</span> : ''
}


export const ToolTip = React.createClass({
  getInitialState: function () {
    return {hover: false};
  },

  mouseOver: function () {
    //this.setState({hover: true});
  },

  mouseOut: function () {
    //this.setState({hover: false});
  },

  onClick: function () {
    this.setState({hover: !this.state.hover});
  },

  render: function() {
    let content = '';
    let active = '';
    if (this.state.hover) {
      content = <div className="tooltip-content">{this.props.text}</div>;
      active = "tooltip-trigger-active";
    }
    return (
      <span className="tooltip">
          <span className={"tooltip-trigger " + active}
                onMouseOver={this.mouseOver}
                onMouseOut={this.mouseOut}
                onClick={this.onClick}
          >?</span>
        {content}
      </span>
    );
  }
});
