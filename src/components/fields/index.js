
import React, { Component } from 'react';

import TextInput from './TextInput';
import DateInput from './DateInput';
import DateTime from './DateTime';
import Select from './Select';
import Radio from './Radio';
import Checkbox from './Checkbox';
import CheckboxGroup from './CheckboxGroup';
import Upload from './Upload';
import CurrencyInput from './CurrencyInput';
import GeoPosition from './GeoPosition';


const UnsupportedField = (props) => {
  return (
    <div>
      <span>Unsupported field: <span>{props.data.type}</span></span>
    </div>
  );
};

/*
 TODO: Add API props external custom components
 */
const COMPONENT_TYPES = {
  "input.text":     TextInput,
  "input.date":     DateInput,
  "input.datetime": DateTime,
  "select":         Select,
  "radio":          Radio,
  "checkbox":       Checkbox,
  "checkbox.group": CheckboxGroup,
  "upload":         Upload,
  "input.currency": CurrencyInput,
  "geoposition":    GeoPosition
};


function getFieldComponent(type) {
  return COMPONENT_TYPES[type] || UnsupportedField;
}

export default getFieldComponent;