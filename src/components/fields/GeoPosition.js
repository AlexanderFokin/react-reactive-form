// dependency https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false
import React, {Component} from 'react';
import {isRequired, ToolTip} from '../helpers/field-helpers'

// get address
// http://maps.googleapis.com/maps/api/geocode/json?latlng=44.4647452,7.3553838&sensor=true

/// Google Map Vars
var map;
var marker;
var mapZoomLevel;

class GeoPosition extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      latitude: props.data.latitude || '',
      longitude: props.data.longitude || ''
    };
  }

  onChange(name) {
    return (event) => {

      const val = event.target.value;
      let v;
      let check = /^\-?\d?\d?(\.{1}\d*)?$/;

      if(check.test(val)){

        if(val == '' || val == '-' || val.charAt(val.length - 1) === '.' || val.charAt(val.length - 1) === '0'){
          v = val
        }else{
          v = parseFloat(val);
        }

        this.setState({
          [name]: v //parseFloat(event.target.value) || event.target.value
        }, () => {

          this.props.onChange(this.state);

          if (typeof this.state.latitude === 'number' && this.state.latitude <= 90 && this.state.latitude >= -90
            && typeof this.state.longitude === 'number' && this.state.longitude <= 90 && this.state.longitude >= -90){
            this.updateMap();
          }
        });

      }

    };
  }

  updateState(latitude, longitude) {
    this.setState({
      latitude: latitude,
      longitude: longitude
    },() => {
      this.props.onChange(this.state);
      this.updateMap();
    } ); // Pass updateMap as a callback
  }

  renderMap(latitude, longitude) {
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: this.props.data.mapZoomLevel || 10,
      disableDefaultUI: true,
      zoomControl: true,
      center: {lat: this.state.latitude, lng: this.state.longitude}
    });

    marker = new google.maps.Marker({
      map: map,
      draggable: true
    });

    google.maps.event.addListener(map, 'click', function (event) {
      var latLng = event.latLng;
      var lat = latLng.lat();
      var lng = latLng.lng();

      this.updateState(lat, lng)
    }.bind(this));

    /// Add an event listener for drag end
    google.maps.event.addListener(marker, 'dragend', function (event) {

      var latLng = event.latLng;
      var lat = latLng.lat();
      var lng = latLng.lng();
      this.updateState(lat, lng)
    }.bind(this));

    map.addListener('zoom_changed', function () {
      mapZoomLevel = map.getZoom();
    });

  }

  // Set map marker position and pan settings
  updateMap(lat, lon) {
    var latLng = new google.maps.LatLng(this.state.latitude, this.state.longitude);
    /// Set a timeout before doing map stuff
    window.setTimeout(function () {
      marker.setPosition(latLng);/// Set the marker position
      map.panTo(latLng); /// Pan map to that position
    }.bind(this), 300);
  }
  
  componentDidMount() {
    if(this.props.data.map.enabled){
      this.renderMap();/// Render a new map
      this.updateState(parseFloat(this.state.latitude), parseFloat(this.state.longitude));/// Run update state, passing in the setup
    }
  }
  
  render() {
    const {latitude, longitude} = this.state;

    let gmap;
    if(this.props.data.map.enabled){
      let mapStyle = {
        width: this.props.data.map.width,
        height: this.props.data.map.height
      };
      gmap = <div id="map" style={mapStyle}></div>;
    }

    let classes = this.props.classes;
    let required = isRequired(this.props.data.required);
    let tooltip = (this.props.data.description) ? <ToolTip text={this.props.data.description}/> : '';

    return (
      <div className={classes}>
        <label className="">{this.props.data.label} {required} {tooltip}</label>
        <table>
          <tbody>
          <tr>
            <td>
              <label>Latitude</label>
              <input type="text" value={latitude} onChange={this.onChange("latitude")}/>
            </td>
            <td>
              <label>Longitude</label>
              <input type="text" value={longitude} onChange={this.onChange("longitude")}/>
            </td>
          </tr>
          </tbody>
        </table>
        {gmap}
      </div>
    );
  }
}


/**/
GeoPosition.defaultProps = {
  latitude: '',
  longitude: '',
  mapZoomLevel: 10,
};

export default GeoPosition;