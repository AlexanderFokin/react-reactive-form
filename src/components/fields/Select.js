
import React, { Component } from 'react';
import {isRequired} from '../helpers/field-helpers'

const Select = (props) => {
  const handleChange = (e) => props.onChange(e.target.value);

  let {data} = props;

  let list;
  if (_.isArray(data.options)) {
    list = _.map(data.options, (value, index) => {
      return <option key={index} value={value.value}>{value.label}</option>
    })
  }

  let required = isRequired(props.data.required);

  return (
    <div className={props.classes}>
      <label htmlFor="">{props.data.label} {required}</label>
      <select className="form-control" required={props.data.required} onChange={handleChange}>
        <option value="">placeholder</option>
        {list}
      </select>
    </div>
  );
};

export default Select;