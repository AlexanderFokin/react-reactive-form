
import React, { Component } from 'react';
import {isRequired} from '../helpers/field-helpers'

const Checkbox = (props) => {
  const handleChange = (e) => props.onChange(e.target.checked);

  let {data, name, state} = props;
  let required = isRequired(props.data.required);

  return (
    <div className={props.classes}>
      <label htmlFor="">
        <input type="checkbox"
               className="form-control"
               {...props}
               name={name}
               checked={props.value}
               onChange={handleChange}
        />
        {props.data.label} {required}
      </label>
    </div>
  );
};

export default Checkbox;