
import React, { Component } from 'react';

const DateInput = (props) => {
  const handleChange = (e) => props.onChange(e.target.value);

  return (
    <div className={props.classes}>
      <label htmlFor="">{props.data.label}</label>
      <input
        {...props.data}
        className="form-control"
        onChange={handleChange}
      />
    </div>
  );
};

export default DateInput;