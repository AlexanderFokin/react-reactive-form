
import React, { Component } from 'react';
import _ from 'lodash';

class Upload extends Component{
  constructor(props){
    super(props);
    this.state = {
      cloudName: this.props.cloudName,
      uploadPreset: this.props.uploadPreset,
      isError: false,
      errorMessage: null,
      showPoweredBy: false,
      allowedFormats: null,
      uuid: this.uuid(),
      imageList: []
    };
  }
  
  uuid (){
    function guid() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
    }
    return guid();
  }

  getUploadOptions (){
    var options = {
      cloud_name: this.state.cloudName,
      upload_preset: this.state.uploadPreset
    };
    options.sources = this.props.sources;
    options.multiple = this.props.multiple;

    if(this.props.maxFiles){
      options.max_files = this.props.maxFiles
    }

    if(this.props.cropping && this.props.cropping === 'server'){
      options.cropping = this.props.cropping;
    }

    if(this.croppingAspectRatio){
      options.cropping_aspect_ratio = this.props.croppingAspectRatio;
    }

    if(this.props.publicId){
      options.public_id = this.props.public_id;
    }

    if(this.props.folder){
      options.folder = this.props.folder;
    }

    if(this.props.tags && this.props.tags.length > 0){
      options.tags = this.props.tags;
    }

    if(this.props.resourceType){
      options.resource_type = this.props.resourceType;
    }

    if(this.props.allowedFormats){
      options.client_allowed_formats = this.props.allowedFormats;
    }

    var context = {};
    if(this.props.contextAlt){
      context.alt = this.props.contextAlt;
    }

    if(this.props.contextCaption){
      context.caption = this.props.contextCaption;
    }

    if(Object.keys(context).length > 0){
      options.context = context;
    }

    return options;
  }

  setError (isError, errorMessage){
    self.setState({
      isError: true,
      errorMessage: 'No result returned from Cloudinary'
    });
  }
  handleClick (ev){
    ev.preventDefault();
    self = this;
    try{
      var options = this.getUploadOptions();
      cloudinary.openUploadWidget(
        options,
        function(error, result) {
          if (error){
            self.setError(true, error)
            return false;
          }
          if (!result || result.length === 0) {
            self.setError(true, 'No result from Cloudinary');
            return false;
          }
          self.setState({
            imageList: self.state.imageList.concat(result)
          }, () => {
            console.log(self.state.imageList);


            var res = _.reduce(self.state.imageList, function(result, value, key) {

              var img = {
                url: value.url,
                thumbnail_url: value.thumbnail_url,
                type: "image",
                format: value.format,
                height: value.height,
                width: value.width,
                created_at: value.created_at,
                signature: value.signature
              };

              result.push(img);
              return result;
            }, []);
            self.props.onChange(res);
          });

          return true;
        }
      );
    } catch(e) {
      self.setError(true, e);
      return false;
    }

  }
  render (){
    var uploader_id = "uploader_" + this.state.uuid;
    var images = this.state.imageList.map((data, i) => {
      return (
        <img
          key={i}
          alt={data.original_filename}
          src={data.url}
          height="100"
          width="100"
        />
      )
    });

    return (
      <section>
        <a ref='uploader' id={uploader_id} href="#"
           className={this.props.buttonClass}
           onClick={this.handleClick.bind(this)}>{this.props.buttonCaption}</a>
        <br/><br/>
        {images}
      </section>
    )
  }
  
}



Upload.defaultProps = {
  showPoweredBy: false,
  sources: ['local', 'url'],
  defaultSource: 'local',
  multiple: true,
  maxFiles: null,
  cropping: null,
  croppingAspectRation: null,
  publicId: null,
  folder: null,
  tags: null,
  resourceType: 'auto',
  contextAlt: null,
  contextCaption: null,
  allowedFormats: ['png', 'gif', 'jpeg'],
  maxFileSize: null,
  maxImageWidth: null,
  maxImageHeight: null,
  buttonClass: 'cloudinary-button',
  buttonCaption: 'Завантажити фото',
  cloudName: 'dkcebtqro',
  uploadPreset: 'qtckofwc'
};

export default Upload;