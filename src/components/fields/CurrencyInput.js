
import React, { Component } from 'react';
import {isRequired, ToolTip} from '../helpers/field-helpers'

const CurrencyInput = (props) => {
  const handleChange = (e) => {

    var regexp = /^[0-9]+([,.][0-9]+)?$/g;
    const str = e.target.value;

    if(regexp.test(str) || str == '' || str.charAt(str.length - 1) === '.'){
      props.onChange(e.target.value);
    }
  };

  let required = isRequired(props.data.required);
  let tooltip = (props.data.description) ? <ToolTip text={props.data.description}/> : '';
  let type = (props.data.hidden) ? 'hidden' : 'text';

  return (
    <div className={props.classes}>
      <label htmlFor="">{props.data.label} {required} {tooltip}</label>
      <input
        type={type}
        value={props.value}
        placeholder={props.data.placeholder}
        required={props.data.required}
        className="form-control"
        onChange={handleChange}
      />
    </div>
  );
};

export default CurrencyInput;