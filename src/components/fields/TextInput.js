
import React, { Component } from 'react';
import {isRequired, ToolTip} from '../helpers/field-helpers'

const TextInput = (props) => {
  const handleChange = (e) => props.onChange(e.target.value);

  //console.log(props);

  let required = isRequired(props.data.required);

  let tooltip = (props.data.description) ? <ToolTip text={props.data.description}/> : '';
  let type = (props.data.hidden) ? 'hidden' : 'text';

  return (
    <div className={props.classes}>
      <label htmlFor="">{props.data.label} {required} {tooltip}</label>
      <input
        type={type}
        placeholder={props.data.placeholder}
        required={props.data.required}
        value={props.value}
        className="form-control"
        onChange={handleChange}
      />
    </div>
  );
};

export default TextInput;