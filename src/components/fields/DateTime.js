
import React, { Component } from 'react';
import MaskedInput from 'react-maskedinput';
import moment from 'moment';
import {isRequired, ToolTip} from '../helpers/field-helpers'

const DateTime = (props) => {

  const handleChange = (e) => {

    var value_f = moment(e.target.value, props.data.format, true);
    var value_u = value_f.unix();

    if(value_f.isValid()){
      props.onChange(value_u);
    }else{
      props.onChange('');
    }

  };
  
  let classes = props.classes;
  let displayValue;
  let dt;

  if(props.value != ''){
    dt = moment.unix(props.value, true);
    if(dt.isValid()){
      displayValue = dt.format(props.data.format);
    }else{
      displayValue = dt.invalidAt();
      classes = classes + " error-datetime";
    }
  }
  
  let required = isRequired(props.data.required);
  let tooltip = (props.data.description) ? <ToolTip text={props.data.description}/> : '';

  return (
    <div className={classes}>
      <label htmlFor="">{props.data.label} {required} {tooltip}</label>
      <span className="date-time-result">{displayValue}</span>
      <MaskedInput
        mask={props.data.mask}
        placeholder={props.data.format}
        placeholderChar=' '
        type="text"
        required={props.data.required}
        onChange={handleChange}
      />

    </div>
  );
};

export default DateTime;
