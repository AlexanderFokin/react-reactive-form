
import React, { Component } from 'react';
import {isRequired} from '../helpers/field-helpers'

const Radio = (props) => {
  const handleChange = (e) => {
    props.onChange(e.target.value);
  };

  let {data, name} = props;

  let list;
  if (_.isArray(data.options)) {
    list = _.map(data.options, (value, index) => {
      return (
        <label htmlFor={name + '_' + index} key={index} value={props.value}>
          <input type="radio" id={name + '_' + index} name={name}
                 value={value.value}
                 checked={props.value == value.value}
                 onChange={handleChange}/>
          {value.label}
        </label>
      )
    })
  }

  let required = isRequired(props.data.required);

  return (
    <div className={props.classes}>
      <label htmlFor="" className="title-radio-group">{props.data.label} {required}</label>
      {list}
    </div>
  );
};

export default Radio;