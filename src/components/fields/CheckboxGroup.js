import React, {Component} from 'react';
import {isRequired} from '../helpers/field-helpers'

const CheckboxGroup = (props) => {

  const handleChange = (e) => {
    var values = [...props.value];
    var checked_values;
    if (e.target.checked) {
      checked_values = values.concat(e.target.value);
    } else {
      checked_values = values.filter(v => v !== e.target.value);
    }
    props.onChange(checked_values);
  };

  let {data, name} = props;
  let checkboxes;
  if (_.isArray(data.options)) {
    checkboxes = _.map(data.options, (value, index) => {
      return (
        <label htmlFor={name + '_' + index} key={index} className="checkbox-group-item">
          <input type="checkbox" id={name + '_' + index} name={value.value}
                 value={value.value}
                 checked={props.value.indexOf(value.value) >= 0}
                 onChange={handleChange}/>
          {value.label}
        </label>
      )
    })
  }

  let required = isRequired(props.data.required);

  return (
    <div className={props.classes}>
      <label className="title-checkbox-group">{props.data.label} {required}</label>
      {checkboxes}
    </div>
  );

};

export default CheckboxGroup;

