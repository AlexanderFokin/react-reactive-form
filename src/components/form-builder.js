
import React, { Component, PropTypes } from 'react';
import _ from 'lodash';
import getFieldComponent from './fields/index';
import {
  getIn, 
  updateIn, 
  createNestedObject, 
  iterate, 
  mergeObjects, 
  getPath,
  fieldClasses,
  changeState,
  defaultDataState,
  setPlaces
} from './utils';


class FormBuilder extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fields: props.schema.fields,
      data: defaultDataState(props.schema.fields),
      places: props.schema.places
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);

    if (this.props.onChange) this.props.onChange(this.state);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      fields: nextProps.schema.fields,
      data: defaultDataState(nextProps.schema.fields),
      places: nextProps.schema.places
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    this.props.onSubmit(this.state);
  }

  handleChange(name, value, _path) {
    if(this.props.debug){
      console.log('handleChange[ name: ' + name + ', value: ' + value + ' path: ' + _path + ' ]');
    }
    
    var path = getPath(_path);

    // form logic
    var field = getIn(this.state, path.field);
    var newStateLogic;
    if(_.has(field, 'onchange')){

      if(typeof field.onchange === "function"){
        newStateLogic = field.onchange(value, this.state);
        changeState(this, this.state, newStateLogic);
      }else if(typeof field.onchange === "string"){

        var onchangeFunction = new Function('value', 'state', field.onchange);
        newStateLogic = onchangeFunction(value, this.state);

        if(this.props.debug){
          console.log('onchangeFunction - >');
          console.log(onchangeFunction);
          console.log('newStateLogic - >');
          console.log(newStateLogic);
        }

        changeState(this, this.state, newStateLogic);
      }

    }

    // update state
    var updaterObject = {...this.state};
    createNestedObject( updaterObject, path.state );
    updaterObject = updateIn(updaterObject, path.state, value);
    var newState = mergeObjects(this.state, updaterObject, false);
    this.setState(newState, () => {
      if (this.props.onChange) this.props.onChange(this.state);
    });
    
  }

  render() {

    let schema = this.props.schema;

    let places = {
      default: []
    };
    var that = this;

    iterate(this.state.fields, '', function (key, path, value) {

      const FieldComponent = getFieldComponent(value.type);

      value.key = key;
      let classes = fieldClasses(value);
      let state_value = getIn(that.state.data, path);

      if(value.enabled){
        
        let tmp = <FieldComponent key={path}
                        name={path}
                        data={value}
                        state={that.state}
                        classes={classes}
                        value={state_value}
                        onChange={value => { that.handleChange(key, value, path);}}/>;


        if(_.has(value, 'place')){
          if(!_.has(places, value.place)){
            places[value.place] = [];
          }
          places[value.place].push(tmp);
        }else {
          places.default.push(tmp);
        }
      }
      
    });

    let form = setPlaces(places, this.state.places);

    if(this.props.debug){
      console.log('places - >');
      console.log(places);
      console.log('form ->');
      console.log(form);
    }

    return (
      <form onSubmit={this.handleSubmit}>
        {this.props.error ? (
          <div className="form-container-error">
            {this.props.error}
          </div>
        ) : null}
        <div className="form-places">
          { form }
          <div className="clear"></div>
        </div>
        <button type="submit" className="button-submit">submit</button>
      </form>
    );
  }
}


export default FormBuilder;