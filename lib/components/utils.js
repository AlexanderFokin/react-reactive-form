'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

exports.getIn = getIn;
exports.updateIn = updateIn;
exports.createNestedObject = createNestedObject;
exports.iterate = iterate;
exports.mergeObjects = mergeObjects;
exports.getPath = getPath;
exports.fieldClasses = fieldClasses;
exports.changeState = changeState;
exports.defaultDataState = defaultDataState;
exports.setPlaces = setPlaces;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getIn(obj, path) {
  var stack;

  if (typeof path == "string") {
    stack = path.split(/[\.\[\]]/g);
  } else {
    stack = path;
  }

  while (stack.length) {
    obj = obj[stack.shift()];
  }

  return obj;
}

function updateIn(obj, path, value) {
  var current = obj;
  var stack;

  if (typeof path == "string") {
    stack = path.split(/[\.\[\]]/g);
  } else {
    stack = path;
  }

  while (stack.length > 1) {
    current = current[stack.shift()];
  }
  current[stack.shift()] = value;

  return obj;
}

function createNestedObject(base, names) {
  for (var i = 0; i < names.length; i++) {
    base = base[names[i]] = base[names[i]] || {};
  }
}

function iterate(obj, stack, callback) {
  for (var property in obj) {
    if (obj.hasOwnProperty(property)) {

      if (obj[property].type) {
        var path = stack ? stack + '.' + property : property;
        callback(property, path, obj[property]);
      } else {
        var path = stack ? stack + '.' + property : property;
        iterate(obj[property], path, callback);
      }
    }
  }
}

function isObject(thing) {
  return (typeof thing === 'undefined' ? 'undefined' : _typeof(thing)) === "object" && thing !== null && !Array.isArray(thing);
}
function mergeObjects(obj1, obj2) {
  var concatArrays = arguments.length <= 2 || arguments[2] === undefined ? false : arguments[2];

  // Recursively merge deeply nested objects.
  var acc = Object.assign({}, obj1); // Prevent mutation of source object.
  return Object.keys(obj2).reduce(function (acc, key) {
    var left = obj1[key],
        right = obj2[key];
    if (obj1.hasOwnProperty(key) && isObject(right)) {
      acc[key] = mergeObjects(left, right, concatArrays);
    } else if (concatArrays && Array.isArray(left) && Array.isArray(right)) {
      acc[key] = left.concat(right);
    } else {
      acc[key] = right;
    }
    return acc;
  }, acc);
}

function getPath(path) {

  var ps = "data." + path;
  var pf = "fields." + path;

  return {
    state: ps.split(/[\.\[\]]/g),
    field: pf.split(/[\.\[\]]/g)
  };
}

function fieldClasses(field) {
  return ['form-group', 'field-' + field.key, 'field-visible-' + field.hidden, 'field-type-' + field.type.replace('.', '_')].join(" ").trim();
}

function changeState(context, state, newState, callback) {
  var ns = mergeObjects(state, newState, false);

  context.setState(ns, function () {
    //if (this.props.onChange) callback(this.state);
  });
}

function defaultDataState(fields) {
  var data = {};
  iterate(fields, '', function (key, path, value) {
    var statePath = path.split(/[\.\[\]]/g);
    createNestedObject(data, statePath);
    if (_lodash2.default.has(value, 'default')) {
      data = updateIn(data, statePath, value.default);
    } else {
      data = updateIn(data, statePath, '');
    }
  });
  return data;
}

// create form parts tabs etc
function setPlaces(places, schemaPlaces) {
  var elements = [];
  _lodash2.default.forEach(places, function (fields, key) {
    var header = void 0;
    var title = void 0;
    var description = void 0;

    if (_lodash2.default.has(schemaPlaces, key)) {
      title = schemaPlaces[key].title ? _react2.default.createElement(
        'div',
        { className: 'place-title' },
        schemaPlaces[key].title
      ) : '';
      description = schemaPlaces[key].description ? _react2.default.createElement(
        'p',
        { className: 'place-description' },
        schemaPlaces[key].description
      ) : '';
      header = _react2.default.createElement(
        'div',
        { className: 'place-header' },
        title,
        ' ',
        description
      );
    }

    elements.push(_react2.default.createElement(
      'div',
      { className: 'form-part ' + key, key: key },
      header,
      fields
    ));
  });
  return elements;
}
//# sourceMappingURL=utils.js.map