'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _index = require('./fields/index');

var _index2 = _interopRequireDefault(_index);

var _utils = require('./utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FormBuilder = function (_Component) {
  _inherits(FormBuilder, _Component);

  function FormBuilder(props) {
    _classCallCheck(this, FormBuilder);

    var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(FormBuilder).call(this, props));

    _this.state = {
      fields: props.schema.fields,
      data: (0, _utils.defaultDataState)(props.schema.fields),
      places: props.schema.places
    };

    _this.handleSubmit = _this.handleSubmit.bind(_this);
    _this.handleChange = _this.handleChange.bind(_this);

    if (_this.props.onChange) _this.props.onChange(_this.state);
    return _this;
  }

  _createClass(FormBuilder, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      this.setState({
        fields: nextProps.schema.fields,
        data: (0, _utils.defaultDataState)(nextProps.schema.fields),
        places: nextProps.schema.places
      });
    }
  }, {
    key: 'handleSubmit',
    value: function handleSubmit(event) {
      event.preventDefault();

      this.props.onSubmit(this.state);
    }
  }, {
    key: 'handleChange',
    value: function handleChange(name, value, _path) {
      var _this2 = this;

      if (this.props.debug) {
        console.log('handleChange[ name: ' + name + ', value: ' + value + ' path: ' + _path + ' ]');
      }

      var path = (0, _utils.getPath)(_path);

      // form logic
      var field = (0, _utils.getIn)(this.state, path.field);
      var newStateLogic;
      if (_lodash2.default.has(field, 'onchange')) {

        if (typeof field.onchange === "function") {
          newStateLogic = field.onchange(value, this.state);
          (0, _utils.changeState)(this, this.state, newStateLogic);
        } else if (typeof field.onchange === "string") {

          var onchangeFunction = new Function('value', 'state', field.onchange);
          newStateLogic = onchangeFunction(value, this.state);

          if (this.props.debug) {
            console.log('onchangeFunction - >');
            console.log(onchangeFunction);
            console.log('newStateLogic - >');
            console.log(newStateLogic);
          }

          (0, _utils.changeState)(this, this.state, newStateLogic);
        }
      }

      // update state
      var updaterObject = _extends({}, this.state);
      (0, _utils.createNestedObject)(updaterObject, path.state);
      updaterObject = (0, _utils.updateIn)(updaterObject, path.state, value);
      var newState = (0, _utils.mergeObjects)(this.state, updaterObject, false);
      this.setState(newState, function () {
        if (_this2.props.onChange) _this2.props.onChange(_this2.state);
      });
    }
  }, {
    key: 'render',
    value: function render() {

      var schema = this.props.schema;

      var places = {
        default: []
      };
      var that = this;

      (0, _utils.iterate)(this.state.fields, '', function (key, path, value) {

        var FieldComponent = (0, _index2.default)(value.type);

        value.key = key;
        var classes = (0, _utils.fieldClasses)(value);
        var state_value = (0, _utils.getIn)(that.state.data, path);

        if (value.enabled) {

          var tmp = _react2.default.createElement(FieldComponent, { key: path,
            name: path,
            data: value,
            state: that.state,
            classes: classes,
            value: state_value,
            onChange: function onChange(value) {
              that.handleChange(key, value, path);
            } });

          if (_lodash2.default.has(value, 'place')) {
            if (!_lodash2.default.has(places, value.place)) {
              places[value.place] = [];
            }
            places[value.place].push(tmp);
          } else {
            places.default.push(tmp);
          }
        }
      });

      var form = (0, _utils.setPlaces)(places, this.state.places);

      if (this.props.debug) {
        console.log('places - >');
        console.log(places);
        console.log('form ->');
        console.log(form);
      }

      return _react2.default.createElement(
        'form',
        { onSubmit: this.handleSubmit },
        this.props.error ? _react2.default.createElement(
          'div',
          { className: 'form-container-error' },
          this.props.error
        ) : null,
        _react2.default.createElement(
          'div',
          { className: 'form-places' },
          form,
          _react2.default.createElement('div', { className: 'clear' })
        ),
        _react2.default.createElement(
          'button',
          { type: 'submit', className: 'button-submit' },
          'submit'
        )
      );
    }
  }]);

  return FormBuilder;
}(_react.Component);

exports.default = FormBuilder;
//# sourceMappingURL=form-builder.js.map