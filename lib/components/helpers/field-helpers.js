'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ToolTip = undefined;
exports.isRequired = isRequired;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function isRequired(required) {
  return required ? _react2.default.createElement(
    'span',
    { className: 'required' },
    '*'
  ) : '';
}

var ToolTip = exports.ToolTip = _react2.default.createClass({
  displayName: 'ToolTip',

  getInitialState: function getInitialState() {
    return { hover: false };
  },

  mouseOver: function mouseOver() {
    //this.setState({hover: true});
  },

  mouseOut: function mouseOut() {
    //this.setState({hover: false});
  },

  onClick: function onClick() {
    this.setState({ hover: !this.state.hover });
  },

  render: function render() {
    var content = '';
    var active = '';
    if (this.state.hover) {
      content = _react2.default.createElement(
        'div',
        { className: 'tooltip-content' },
        this.props.text
      );
      active = "tooltip-trigger-active";
    }
    return _react2.default.createElement(
      'span',
      { className: 'tooltip' },
      _react2.default.createElement(
        'span',
        { className: "tooltip-trigger " + active,
          onMouseOver: this.mouseOver,
          onMouseOut: this.mouseOut,
          onClick: this.onClick
        },
        '?'
      ),
      content
    );
  }
});
//# sourceMappingURL=field-helpers.js.map