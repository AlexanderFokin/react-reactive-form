'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _TextInput = require('./TextInput');

var _TextInput2 = _interopRequireDefault(_TextInput);

var _DateInput = require('./DateInput');

var _DateInput2 = _interopRequireDefault(_DateInput);

var _DateTime = require('./DateTime');

var _DateTime2 = _interopRequireDefault(_DateTime);

var _Select = require('./Select');

var _Select2 = _interopRequireDefault(_Select);

var _Radio = require('./Radio');

var _Radio2 = _interopRequireDefault(_Radio);

var _Checkbox = require('./Checkbox');

var _Checkbox2 = _interopRequireDefault(_Checkbox);

var _CheckboxGroup = require('./CheckboxGroup');

var _CheckboxGroup2 = _interopRequireDefault(_CheckboxGroup);

var _Upload = require('./Upload');

var _Upload2 = _interopRequireDefault(_Upload);

var _CurrencyInput = require('./CurrencyInput');

var _CurrencyInput2 = _interopRequireDefault(_CurrencyInput);

var _GeoPosition = require('./GeoPosition');

var _GeoPosition2 = _interopRequireDefault(_GeoPosition);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var UnsupportedField = function UnsupportedField(props) {
  return _react2.default.createElement(
    'div',
    null,
    _react2.default.createElement(
      'span',
      null,
      'Unsupported field: ',
      _react2.default.createElement(
        'span',
        null,
        props.data.type
      )
    )
  );
};

/*
 TODO: Add API props external custom components
 */
var COMPONENT_TYPES = {
  "input.text": _TextInput2.default,
  "input.date": _DateInput2.default,
  "input.datetime": _DateTime2.default,
  "select": _Select2.default,
  "radio": _Radio2.default,
  "checkbox": _Checkbox2.default,
  "checkbox.group": _CheckboxGroup2.default,
  "upload": _Upload2.default,
  "input.currency": _CurrencyInput2.default,
  "geoposition": _GeoPosition2.default
};

function getFieldComponent(type) {
  return COMPONENT_TYPES[type] || UnsupportedField;
}

exports.default = getFieldComponent;
//# sourceMappingURL=index.js.map