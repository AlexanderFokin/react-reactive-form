'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _fieldHelpers = require('../helpers/field-helpers');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Checkbox = function Checkbox(props) {
  var handleChange = function handleChange(e) {
    return props.onChange(e.target.checked);
  };

  var data = props.data;
  var name = props.name;
  var state = props.state;

  var required = (0, _fieldHelpers.isRequired)(props.data.required);

  return _react2.default.createElement(
    'div',
    { className: props.classes },
    _react2.default.createElement(
      'label',
      { htmlFor: '' },
      _react2.default.createElement('input', _extends({ type: 'checkbox',
        className: 'form-control'
      }, props, {
        name: name,
        checked: props.value,
        onChange: handleChange
      })),
      props.data.label,
      ' ',
      required
    )
  );
};

exports.default = Checkbox;
//# sourceMappingURL=Checkbox.js.map