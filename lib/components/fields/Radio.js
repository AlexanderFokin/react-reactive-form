'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _fieldHelpers = require('../helpers/field-helpers');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Radio = function Radio(props) {
  var handleChange = function handleChange(e) {
    props.onChange(e.target.value);
  };

  var data = props.data;
  var name = props.name;


  var list = void 0;
  if (_.isArray(data.options)) {
    list = _.map(data.options, function (value, index) {
      return _react2.default.createElement(
        'label',
        { htmlFor: name + '_' + index, key: index, value: props.value },
        _react2.default.createElement('input', { type: 'radio', id: name + '_' + index, name: name,
          value: value.value,
          checked: props.value == value.value,
          onChange: handleChange }),
        value.label
      );
    });
  }

  var required = (0, _fieldHelpers.isRequired)(props.data.required);

  return _react2.default.createElement(
    'div',
    { className: props.classes },
    _react2.default.createElement(
      'label',
      { htmlFor: '', className: 'title-radio-group' },
      props.data.label,
      ' ',
      required
    ),
    list
  );
};

exports.default = Radio;
//# sourceMappingURL=Radio.js.map