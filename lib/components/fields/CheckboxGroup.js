'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _fieldHelpers = require('../helpers/field-helpers');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var CheckboxGroup = function CheckboxGroup(props) {

  var handleChange = function handleChange(e) {
    var values = [].concat(_toConsumableArray(props.value));
    var checked_values;
    if (e.target.checked) {
      checked_values = values.concat(e.target.value);
    } else {
      checked_values = values.filter(function (v) {
        return v !== e.target.value;
      });
    }
    props.onChange(checked_values);
  };

  var data = props.data;
  var name = props.name;

  var checkboxes = void 0;
  if (_.isArray(data.options)) {
    checkboxes = _.map(data.options, function (value, index) {
      return _react2.default.createElement(
        'label',
        { htmlFor: name + '_' + index, key: index, className: 'checkbox-group-item' },
        _react2.default.createElement('input', { type: 'checkbox', id: name + '_' + index, name: value.value,
          value: value.value,
          checked: props.value.indexOf(value.value) >= 0,
          onChange: handleChange }),
        value.label
      );
    });
  }

  var required = (0, _fieldHelpers.isRequired)(props.data.required);

  return _react2.default.createElement(
    'div',
    { className: props.classes },
    _react2.default.createElement(
      'label',
      { className: 'title-checkbox-group' },
      props.data.label,
      ' ',
      required
    ),
    checkboxes
  );
};

exports.default = CheckboxGroup;
//# sourceMappingURL=CheckboxGroup.js.map