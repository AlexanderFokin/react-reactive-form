'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _fieldHelpers = require('../helpers/field-helpers');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TextInput = function TextInput(props) {
  var handleChange = function handleChange(e) {
    return props.onChange(e.target.value);
  };

  //console.log(props);

  var required = (0, _fieldHelpers.isRequired)(props.data.required);

  var tooltip = props.data.description ? _react2.default.createElement(_fieldHelpers.ToolTip, { text: props.data.description }) : '';
  var type = props.data.hidden ? 'hidden' : 'text';

  return _react2.default.createElement(
    'div',
    { className: props.classes },
    _react2.default.createElement(
      'label',
      { htmlFor: '' },
      props.data.label,
      ' ',
      required,
      ' ',
      tooltip
    ),
    _react2.default.createElement('input', {
      type: type,
      placeholder: props.data.placeholder,
      required: props.data.required,
      value: props.value,
      className: 'form-control',
      onChange: handleChange
    })
  );
};

exports.default = TextInput;
//# sourceMappingURL=TextInput.js.map