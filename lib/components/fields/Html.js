"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Html = function Html(props) {
  var handleChange = function handleChange(e) {
    return props.onChange(e.target.value);
  };

  return _react2.default.createElement(
    "div",
    { className: "html" },
    _react2.default.createElement("div", null)
  );
};

exports.default = Html;
//# sourceMappingURL=Html.js.map