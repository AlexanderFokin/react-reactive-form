'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Upload = function (_Component) {
  _inherits(Upload, _Component);

  function Upload(props) {
    _classCallCheck(this, Upload);

    var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(Upload).call(this, props));

    _this.state = {
      cloudName: _this.props.cloudName,
      uploadPreset: _this.props.uploadPreset,
      isError: false,
      errorMessage: null,
      showPoweredBy: false,
      allowedFormats: null,
      uuid: _this.uuid(),
      imageList: []
    };
    return _this;
  }

  _createClass(Upload, [{
    key: 'uuid',
    value: function uuid() {
      function guid() {
        function s4() {
          return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
      }
      return guid();
    }
  }, {
    key: 'getUploadOptions',
    value: function getUploadOptions() {
      var options = {
        cloud_name: this.state.cloudName,
        upload_preset: this.state.uploadPreset
      };
      options.sources = this.props.sources;
      options.multiple = this.props.multiple;

      if (this.props.maxFiles) {
        options.max_files = this.props.maxFiles;
      }

      if (this.props.cropping && this.props.cropping === 'server') {
        options.cropping = this.props.cropping;
      }

      if (this.croppingAspectRatio) {
        options.cropping_aspect_ratio = this.props.croppingAspectRatio;
      }

      if (this.props.publicId) {
        options.public_id = this.props.public_id;
      }

      if (this.props.folder) {
        options.folder = this.props.folder;
      }

      if (this.props.tags && this.props.tags.length > 0) {
        options.tags = this.props.tags;
      }

      if (this.props.resourceType) {
        options.resource_type = this.props.resourceType;
      }

      if (this.props.allowedFormats) {
        options.client_allowed_formats = this.props.allowedFormats;
      }

      var context = {};
      if (this.props.contextAlt) {
        context.alt = this.props.contextAlt;
      }

      if (this.props.contextCaption) {
        context.caption = this.props.contextCaption;
      }

      if (Object.keys(context).length > 0) {
        options.context = context;
      }

      return options;
    }
  }, {
    key: 'setError',
    value: function setError(isError, errorMessage) {
      self.setState({
        isError: true,
        errorMessage: 'No result returned from Cloudinary'
      });
    }
  }, {
    key: 'handleClick',
    value: function handleClick(ev) {
      ev.preventDefault();
      self = this;
      try {
        var options = this.getUploadOptions();
        cloudinary.openUploadWidget(options, function (error, result) {
          if (error) {
            self.setError(true, error);
            return false;
          }
          if (!result || result.length === 0) {
            self.setError(true, 'No result from Cloudinary');
            return false;
          }
          self.setState({
            imageList: self.state.imageList.concat(result)
          }, function () {
            console.log(self.state.imageList);

            var res = _lodash2.default.reduce(self.state.imageList, function (result, value, key) {

              var img = {
                url: value.url,
                thumbnail_url: value.thumbnail_url,
                type: "image",
                format: value.format,
                height: value.height,
                width: value.width,
                created_at: value.created_at,
                signature: value.signature
              };

              result.push(img);
              return result;
            }, []);
            self.props.onChange(res);
          });

          return true;
        });
      } catch (e) {
        self.setError(true, e);
        return false;
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var uploader_id = "uploader_" + this.state.uuid;
      var images = this.state.imageList.map(function (data, i) {
        return _react2.default.createElement('img', {
          key: i,
          alt: data.original_filename,
          src: data.url,
          height: '100',
          width: '100'
        });
      });

      return _react2.default.createElement(
        'section',
        null,
        _react2.default.createElement(
          'a',
          { ref: 'uploader', id: uploader_id, href: '#',
            className: this.props.buttonClass,
            onClick: this.handleClick.bind(this) },
          this.props.buttonCaption
        ),
        _react2.default.createElement('br', null),
        _react2.default.createElement('br', null),
        images
      );
    }
  }]);

  return Upload;
}(_react.Component);

Upload.defaultProps = {
  showPoweredBy: false,
  sources: ['local', 'url'],
  defaultSource: 'local',
  multiple: true,
  maxFiles: null,
  cropping: null,
  croppingAspectRation: null,
  publicId: null,
  folder: null,
  tags: null,
  resourceType: 'auto',
  contextAlt: null,
  contextCaption: null,
  allowedFormats: ['png', 'gif', 'jpeg'],
  maxFileSize: null,
  maxImageWidth: null,
  maxImageHeight: null,
  buttonClass: 'cloudinary-button',
  buttonCaption: 'Завантажити фото',
  cloudName: 'dkcebtqro',
  uploadPreset: 'qtckofwc'
};

exports.default = Upload;
//# sourceMappingURL=Upload.js.map