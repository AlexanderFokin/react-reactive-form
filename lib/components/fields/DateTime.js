'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactMaskedinput = require('react-maskedinput');

var _reactMaskedinput2 = _interopRequireDefault(_reactMaskedinput);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _fieldHelpers = require('../helpers/field-helpers');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DateTime = function DateTime(props) {

  var handleChange = function handleChange(e) {

    var value_f = (0, _moment2.default)(e.target.value, props.data.format, true);
    var value_u = value_f.unix();

    if (value_f.isValid()) {
      props.onChange(value_u);
    } else {
      props.onChange('');
    }
  };

  var classes = props.classes;
  var displayValue = void 0;
  var dt = void 0;

  if (props.value != '') {
    dt = _moment2.default.unix(props.value, true);
    if (dt.isValid()) {
      displayValue = dt.format(props.data.format);
    } else {
      displayValue = dt.invalidAt();
      classes = classes + " error-datetime";
    }
  }

  var required = (0, _fieldHelpers.isRequired)(props.data.required);
  var tooltip = props.data.description ? _react2.default.createElement(_fieldHelpers.ToolTip, { text: props.data.description }) : '';

  return _react2.default.createElement(
    'div',
    { className: classes },
    _react2.default.createElement(
      'label',
      { htmlFor: '' },
      props.data.label,
      ' ',
      required,
      ' ',
      tooltip
    ),
    _react2.default.createElement(
      'span',
      { className: 'date-time-result' },
      displayValue
    ),
    _react2.default.createElement(_reactMaskedinput2.default, {
      mask: props.data.mask,
      placeholder: props.data.format,
      placeholderChar: ' ',
      type: 'text',
      required: props.data.required,
      onChange: handleChange
    })
  );
};

exports.default = DateTime;
//# sourceMappingURL=DateTime.js.map