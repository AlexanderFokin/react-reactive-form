'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _fieldHelpers = require('../helpers/field-helpers');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // dependency https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false


// get address
// http://maps.googleapis.com/maps/api/geocode/json?latlng=44.4647452,7.3553838&sensor=true

/// Google Map Vars
var map;
var marker;
var mapZoomLevel;

var GeoPosition = function (_React$Component) {
  _inherits(GeoPosition, _React$Component);

  function GeoPosition(props) {
    _classCallCheck(this, GeoPosition);

    var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(GeoPosition).call(this, props));

    _this.state = {
      latitude: props.data.latitude || '',
      longitude: props.data.longitude || ''
    };
    return _this;
  }

  _createClass(GeoPosition, [{
    key: 'onChange',
    value: function onChange(name) {
      var _this2 = this;

      return function (event) {

        var val = event.target.value;
        var v = void 0;
        var check = /^\-?\d?\d?(\.{1}\d*)?$/;

        if (check.test(val)) {

          if (val == '' || val == '-' || val.charAt(val.length - 1) === '.' || val.charAt(val.length - 1) === '0') {
            v = val;
          } else {
            v = parseFloat(val);
          }

          _this2.setState(_defineProperty({}, name, v), //parseFloat(event.target.value) || event.target.value
          function () {

            _this2.props.onChange(_this2.state);

            if (typeof _this2.state.latitude === 'number' && _this2.state.latitude <= 90 && _this2.state.latitude >= -90 && typeof _this2.state.longitude === 'number' && _this2.state.longitude <= 90 && _this2.state.longitude >= -90) {
              _this2.updateMap();
            }
          });
        }
      };
    }
  }, {
    key: 'updateState',
    value: function updateState(latitude, longitude) {
      var _this3 = this;

      this.setState({
        latitude: latitude,
        longitude: longitude
      }, function () {
        _this3.props.onChange(_this3.state);
        _this3.updateMap();
      }); // Pass updateMap as a callback
    }
  }, {
    key: 'renderMap',
    value: function renderMap(latitude, longitude) {
      map = new google.maps.Map(document.getElementById('map'), {
        zoom: this.props.data.mapZoomLevel || 10,
        disableDefaultUI: true,
        zoomControl: true,
        center: { lat: this.state.latitude, lng: this.state.longitude }
      });

      marker = new google.maps.Marker({
        map: map,
        draggable: true
      });

      google.maps.event.addListener(map, 'click', function (event) {
        var latLng = event.latLng;
        var lat = latLng.lat();
        var lng = latLng.lng();

        this.updateState(lat, lng);
      }.bind(this));

      /// Add an event listener for drag end
      google.maps.event.addListener(marker, 'dragend', function (event) {

        var latLng = event.latLng;
        var lat = latLng.lat();
        var lng = latLng.lng();
        this.updateState(lat, lng);
      }.bind(this));

      map.addListener('zoom_changed', function () {
        mapZoomLevel = map.getZoom();
      });
    }

    // Set map marker position and pan settings

  }, {
    key: 'updateMap',
    value: function updateMap(lat, lon) {
      var latLng = new google.maps.LatLng(this.state.latitude, this.state.longitude);
      /// Set a timeout before doing map stuff
      window.setTimeout(function () {
        marker.setPosition(latLng); /// Set the marker position
        map.panTo(latLng); /// Pan map to that position
      }.bind(this), 300);
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (this.props.data.map.enabled) {
        this.renderMap(); /// Render a new map
        this.updateState(parseFloat(this.state.latitude), parseFloat(this.state.longitude)); /// Run update state, passing in the setup
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _state = this.state;
      var latitude = _state.latitude;
      var longitude = _state.longitude;


      var gmap = void 0;
      if (this.props.data.map.enabled) {
        var mapStyle = {
          width: this.props.data.map.width,
          height: this.props.data.map.height
        };
        gmap = _react2.default.createElement('div', { id: 'map', style: mapStyle });
      }

      var classes = this.props.classes;
      var required = (0, _fieldHelpers.isRequired)(this.props.data.required);
      var tooltip = this.props.data.description ? _react2.default.createElement(_fieldHelpers.ToolTip, { text: this.props.data.description }) : '';

      return _react2.default.createElement(
        'div',
        { className: classes },
        _react2.default.createElement(
          'label',
          { className: '' },
          this.props.data.label,
          ' ',
          required,
          ' ',
          tooltip
        ),
        _react2.default.createElement(
          'table',
          null,
          _react2.default.createElement(
            'tbody',
            null,
            _react2.default.createElement(
              'tr',
              null,
              _react2.default.createElement(
                'td',
                null,
                _react2.default.createElement(
                  'label',
                  null,
                  'Latitude'
                ),
                _react2.default.createElement('input', { type: 'text', value: latitude, onChange: this.onChange("latitude") })
              ),
              _react2.default.createElement(
                'td',
                null,
                _react2.default.createElement(
                  'label',
                  null,
                  'Longitude'
                ),
                _react2.default.createElement('input', { type: 'text', value: longitude, onChange: this.onChange("longitude") })
              )
            )
          )
        ),
        gmap
      );
    }
  }]);

  return GeoPosition;
}(_react2.default.Component);

/**/


GeoPosition.defaultProps = {
  latitude: '',
  longitude: '',
  mapZoomLevel: 10
};

exports.default = GeoPosition;
//# sourceMappingURL=GeoPosition.js.map