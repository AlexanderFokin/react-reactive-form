"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DateInput = function DateInput(props) {
  var handleChange = function handleChange(e) {
    return props.onChange(e.target.value);
  };

  return _react2.default.createElement(
    "div",
    { className: props.classes },
    _react2.default.createElement(
      "label",
      { htmlFor: "" },
      props.data.label
    ),
    _react2.default.createElement("input", _extends({}, props.data, {
      className: "form-control",
      onChange: handleChange
    }))
  );
};

exports.default = DateInput;
//# sourceMappingURL=DateInput.js.map