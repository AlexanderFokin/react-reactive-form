'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _fieldHelpers = require('../helpers/field-helpers');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Select = function Select(props) {
  var handleChange = function handleChange(e) {
    return props.onChange(e.target.value);
  };

  var data = props.data;


  var list = void 0;
  if (_.isArray(data.options)) {
    list = _.map(data.options, function (value, index) {
      return _react2.default.createElement(
        'option',
        { key: index, value: value.value },
        value.label
      );
    });
  }

  var required = (0, _fieldHelpers.isRequired)(props.data.required);

  return _react2.default.createElement(
    'div',
    { className: props.classes },
    _react2.default.createElement(
      'label',
      { htmlFor: '' },
      props.data.label,
      ' ',
      required
    ),
    _react2.default.createElement(
      'select',
      { className: 'form-control', required: props.data.required, onChange: handleChange },
      _react2.default.createElement(
        'option',
        { value: '' },
        'placeholder'
      ),
      list
    )
  );
};

exports.default = Select;
//# sourceMappingURL=Select.js.map