'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _formBuilder = require('./components/form-builder');

var _formBuilder2 = _interopRequireDefault(_formBuilder);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _formBuilder2.default;
//# sourceMappingURL=index.js.map